#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include <time.h>
#include <unistd.h>

#define SERVER_IP "192.168.0.225"
#define SERVER_PORT 4000
#define FPS 12
#define FRAME_INTERVAL_MS (1000 / FPS)
#define MIN_FPS 10
#define MIN_FRAME_INTERVAL_MS (1000 / MIN_FPS)

// Helps with logging
unsigned long long currentTimeMillis() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (ts.tv_sec * 1000ULL) + (ts.tv_nsec / 1000000ULL);
}

void sendFrame(int frameNumber, int colorIndex) {
    CURL *curl = curl_easy_init();
    if (!curl) {
        fprintf(stderr, "Failed to initialize CURL\n");
        return;
    }

    int red = 0, green = 0, blue = 0;
    switch (colorIndex % 3) {
        case 0: red = 255; break;
        case 1: green = 255; break;
        case 2: blue = 255; break;
    }

    char jsonPayload[512];
    snprintf(jsonPayload, sizeof(jsonPayload),
        "{"
        "\"frameNumber\": %d,"
        "\"syncTimestamp\": %llu,"
        "\"frames\": ["
        "   {"
        "       \"piID\": \"pi1\","
        "       \"pixels\": [{\"x\":1,\"y\":1,\"color\":[%d,%d,%d]}]"
        "   }"
        "]"
        "}",
        frameNumber, currentTimeMillis(), red, green, blue);

    char url[256];
    snprintf(url, sizeof(url), "http://%s:%d/api/sweep3d", SERVER_IP, SERVER_PORT);

    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, jsonPayload);

    curl_easy_perform(curl);

    curl_slist_free_all(headers);
    curl_easy_cleanup(curl);
}

int main() {
    int frameNumber = 0;

    // T0
    struct timespec startTime;
    clock_gettime(CLOCK_MONOTONIC, &startTime);

    while (frameNumber < 100) {
        struct timespec nextFrameTime = startTime;

        long long offsetNs = frameNumber * FRAME_INTERVAL_MS * 1000000LL;
        nextFrameTime.tv_nsec += offsetNs;

        // Normalize if we overflow nanoseconds into seconds
        while (nextFrameTime.tv_nsec >= 1000000000) {
            nextFrameTime.tv_sec++;
            nextFrameTime.tv_nsec -= 1000000000;
        }

        sendFrame(frameNumber, frameNumber);
        clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &nextFrameTime, NULL);

        // Logging for debugging
        if (frameNumber > 0) {
            static unsigned long long lastTimestamp = 0;
            unsigned long long now = currentTimeMillis();
            if (lastTimestamp != 0) {
                printf("Frame %d interval: %llu ms\n", frameNumber, now - lastTimestamp);
            }
            lastTimestamp = now;
        }

        frameNumber++;
    }

    return 0;
}
