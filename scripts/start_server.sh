#!/bin/bash

# Permission issues? Run `chmod +x start_server.sh` first to make this file executable

# Activate virtual environment
source ./venv/bin/activate

# Start the server
npx ts-node src/index.ts

# Optionally, deactivate the virtual environment after the server stops
deactivate
