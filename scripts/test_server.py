#!/bin/bash
#!/bin/bash

# Navigate to the server directory and start the server
cd server
echo "Starting server..."
npx ts-node src/index.ts &
SERVER_PID=$!

# Navigate to the client directory and start the client
cd ../client
echo "Starting client..."
npx ts-node src/app.ts &
CLIENT_PID=$!

# Wait a few seconds to ensure both server and client are started
sleep 10

# Test the server with curl commands
echo "Testing server endpoints..."

# Replace localhost with the appropriate IP address or hostname if needed
SERVER_URL="http://localhost:4000/api"

echo "Testing light on..."
curl -X POST "$SERVER_URL/light/on"
echo ""

echo "Testing light off..."
curl -X POST "$SERVER_URL/light/off"
echo ""

echo "Testing motor move to position 50..."
curl -X POST "$SERVER_URL/motor/move" -H "Content-Type: application/json" -d '{"position": 50}'
echo ""

echo "Testing motor stop..."
curl -X POST "$SERVER_URL/motor/stop"
echo ""

# Stop the server and client
echo "Stopping server and client..."
kill $SERVER_PID
kill $CLIENT_PID

echo "Done."

