# Ansible Setup Guide

This guide will help you set up Ansible on your machine, install sshpass for SSH password authentication, connect to your devices to add their SSH fingerprints, and run an Ansible playbook.

## About Ansible

Ansible is an open-source automation tool used for configuration management, application deployment, and task automation. It allows you to manage multiple servers from a central location, making it easier to maintain consistency across your infrastructure.

## Prerequisites

-   A Linux-based operating system (For Windows, use the Windows Subsystem for Linux and download a Linux distribution i.e. Ubuntu or Debian)
-   SSH access to the devices you want to manage with Ansible

## Step 1: Install Ansible

1. Update your package list:

```bash
sudo apt update
```

2. Install Ansible:

```bash
sudo apt install ansible
```

3. Verify the installation:

```bash
ansible --version
```

## Step 2: Install `sshpass`

Install: `sshpass` is required if you want to use SSH password authentication with Ansible.

```bash
sudo apt install sshpass
```

## Step 3: Connect to Devices

Before running Ansible, connect to each device using SSH to add their fingerprints to your known_hosts file. Replace user@device_ip with the appropriate username and IP address for each device:

```bash
ssh user@device_ip
```

Type yes when prompted to add the fingerprint. You can exit the SSH session after the fingerprint is added.

## Step 4: Run the Ansible Playbook

Run the playbook:

```bash
ansible-playbook -i inventory.ini playbooks/test_playbook.yml
```

This will execute the ping module on each device, which checks connectivity.
