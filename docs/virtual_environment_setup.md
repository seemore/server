# Setting Up Your Python 3 Virtual Environment

---

Python virtual environments allows us to maintain a single environment configuration across members.
It also allows us to define multiple configurations of python should we need to test anything under a variety of situations.

## Prerequisites

Python 3

## Installation

##### **_On Windows, Python 3.x might be defined as `python`. In that case, replace all occurences of `python3` with `python`_**

Make sure the `virtualenv` tool is installed

```
$ python3 -m pip install virtualenv
```

Create a virtual environment.

> If you're here from the CSG Primer, make sure you do this command in the csg folder, NOT the CSGenome folder

```
$ python3 -m virtualenv .venv
```

#### Starting/Stopping the environment

Activating:

1. Windows

```sh
$ . .venv/Scripts/activate
```

2. Linux

```sh
$ . .venv/bin/activate
```

> the period at the begining of the above commands is shorthand for 'source' ([more info here](<https://en.wikipedia.org/wiki/Dot_(command)#Source>)).

Deactivating:

The environment can be deactivated at any time with `deactivate`

```sh
$ deactivate
```

#### Installing requirements

From a base installation you should see only 2 packages when running `pip list`

To install all the packages used by our codebase, run the following:

```sh
$ pip install -r requirements.txt
```
