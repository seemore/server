# Server

Server communicating between the RaspberryPis and then game engine.

## Getting Started

See the [Installation Guide](/docs/installation_guide.md).

First, compile and run the development server:

```bash
npm run dev
```

## FAQ

### How do I update the code on the Pis?

1. [Complete the Ansible setup guide.](/docs/ansible_setup.md)
2. Run the playbook:

```bash
ansible-playbook -i inventory.ini playbooks/pull_client_repo.yml
```

### I'm getting a permissions error when trying to pull the code to a client device

client@raspberrypi:~/client $ git pull
Updating a4e3be8..bdaf5ee
error: unable to unlink old 'src/services/LightService.ts': Permission denied
client@raspberrypi:~/client $ sudo chown -R $USER:$USER .

```bash
sudo chown -R $USER:$USER .
```


### How can I set the hostname + ID for a device?

```bash
ansible-playbook -i setup_inventory.ini playbooks/configure_client_identifier.yml --extra-vars "ID=02 hostname=alpha02"
```