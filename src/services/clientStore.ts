import { DeviceInfo } from '../constants'
import { getAnsibleHosts } from '../socket/handlers/getAnsibleHosts'
import { Server as SocketIOServer } from 'socket.io'
import { Rooms } from '../constants'
import merge from 'deepmerge'

class ClientStore {
    private hostList: string[] = []
    private connectedClients: DeviceInfo[] = []

    public async init() {
        // Parses and returns client inventory host file:  [ 'alpha01.local', 'alpha02.local', ...]

        this.hostList = await getAnsibleHosts() // Wait for the asynchronous function to complete

        this.connectedClients = this.hostList.map((hostname) => ({
            hostname: hostname.replace('.local', ''), // Create a DeviceInfo object with 'id' field
        }))
    }

    public async refreshConnectedClients() {
        // Fetch the updated host list
        this.hostList = await getAnsibleHosts()

        // Create a temporary array of connected clients
        const tempConnectedClients = this.connectedClients

        // Create a new list of connected clients based on the new host list
        const updatedConnectedClients = this.hostList.map((hostname) => ({
            hostname: hostname.replace('.local', ''), // Remove .local from hostname
        }))

        // Update existing connected clients if they exist in the new list
        updatedConnectedClients.forEach((client) => {
            // Find the existing client in tempConnectedClients by hostname
            const existingClient = tempConnectedClients.find(
                (c) => c.hostname === client.hostname
            )

            if (existingClient) {
                // If the client exists, update its properties
                Object.assign(client, existingClient) // Merge the data
            }
        })

        // Finally, assign the updated clients list back to connectedClients
        this.connectedClients = updatedConnectedClients
    }

    // Returns the list of clients
    public getConnectedClients(): DeviceInfo[] {
        return this.connectedClients
    }

    // Returns the client inventory host file:  [ 'alpha01', 'alpha02', ...]
    public getHostList(): string[] {
        return this.hostList.map((hostname) => hostname.replace('.local', ''))
    }

    // Find a client by socket ID
    public findClientBySocketID(socketID: string): DeviceInfo | undefined {
        return this.connectedClients.find(
            (client) => client.socketID === socketID
        )
    }

    // Find a client by hostname
    public findClientByHostname(hostname: string): DeviceInfo | undefined {
        return this.connectedClients.find(
            (client) => client.hostname === hostname
        )
    }

    // Update client info by hostname or socket ID
    public updateClientBySocketID(
        socketID: string,
        clientInfo: Partial<DeviceInfo>
    ): void {
        const clientIndex = this.connectedClients.findIndex(
            (client) => client.socketID === socketID
        )

        const client = this.connectedClients[clientIndex]
        
        if (clientIndex !== -1) {
            // Push new logs
            if (clientInfo.logs && clientInfo.logs.length > 0) {

                if (!client.logs) {
                    client.logs = [];
                }
    
                client.logs.push(...clientInfo.logs);

                if (client.logs.length > 50) {
                    client.logs = client.logs.slice(-50);
                }

                delete clientInfo.logs;
            }

            // Merge all other data
            this.connectedClients[clientIndex] = merge(client, clientInfo)
        }
    }

    // Update client info by socket ID
    public updateClientByHostname(
        hostname: string,
        clientInfo: Partial<DeviceInfo>
    ): void {
        const clientIndex = this.connectedClients.findIndex(
            (client) => client.hostname === hostname
        )

        if (clientIndex !== -1) {
            this.connectedClients[clientIndex] = merge(
                this.connectedClients[clientIndex],
                clientInfo
            )
        }
    }

    public broadcastClientsToAdmins(io: SocketIOServer): void {
        io.to(Rooms.Admins).emit(
            'clients:list',
            clientStore.getConnectedClients()
        )
    }
}

const clientStore = new ClientStore()
export default clientStore
