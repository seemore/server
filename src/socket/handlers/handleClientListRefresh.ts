import { Socket } from 'socket.io'
import { Server as SocketIOServer } from 'socket.io'
import clientStore from '../../services/clientStore'

export async function handleClientListRefresh(
    io: SocketIOServer,
    socket: Socket
): Promise<void> {
    await clientStore.refreshConnectedClients()
    //TODO: Only send this to clients that need it (more than just the client that requested it)
    clientStore.broadcastClientsToAdmins(io)
}
