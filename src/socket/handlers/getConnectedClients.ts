import { Server as SocketIOServer } from 'socket.io'

export async function getConnectedClients(io: SocketIOServer): Promise<
    {
        id: string
        ip: string
        connectedAt: string
        connected: boolean
        userAgent: string | undefined
    }[]
> {
    try {
        // Fetch all connected clients (sockets)
        const clients = await io.fetchSockets()

        // Extract the necessary information to avoid circular reference issues
        const clientList = clients.map((s) => ({
            id: s.id,
            ip: s.handshake.address,
            connectedAt: s.handshake.time,
            connected: true,
            userAgent: s.handshake.headers['user-agent'],
        }))

        return clientList
    } catch (error) {
        console.error('Error fetching client list:', error)
        return []
    }
}
