import { exec } from 'child_process'

export function runAnsiblePlaybook({
    playbook,
    hosts,
    callback,
}: {
    playbook: string
    hosts: string[]
    callback: (response: {
        error?: string
        success?: boolean
        output?: string
    }) => void
}) {
    if (callback === undefined) {
        return console.error('Callback function is required')
    }

    if (!Array.isArray(hosts) || hosts.length === 0) {
        return callback({ error: 'Invalid host data' }) // Acknowledge with error
    }

    const hostList = hosts.join(',')
    const inventoryList = 'inventory.ini'
    const command = `ansible-playbook ${playbook} -i ${inventoryList} -l ${hostList}`

    console.log(`Running ${playbook} on hosts: ${hostList}`)

    exec(command, (error, stdout, stderr) => {
        if (error) {
            console.error(`${playbook} playbook error:`, error)
            callback({ error: stderr || error.message })
        } else {
            console.error(`${playbook} playbook output:`, stdout)
            callback({ success: true, output: stdout })
        }
    })
}
