import { Server as SocketIOServer, Socket } from 'socket.io'
import clientStore from '../../services/clientStore'

export function handleDisconnection(io: SocketIOServer, socket: Socket): void {
    console.log(`Client Disconnected: (${socket.id})`)

    const client = clientStore.findClientBySocketID(socket.id)

    // Update client with new information
    if (client) {
        clientStore.updateClientBySocketID(socket.id, {
            connectionStatus: 'disconnected',
            connectedAt: undefined,
        })
        clientStore.broadcastClientsToAdmins(io)
    }
}
