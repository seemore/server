/* eslint-disable @typescript-eslint/no-explicit-any */
import { Server } from 'socket.io'
import { Rooms } from '../../constants'

export const emitEventToClients = (
    io: Server,
    targetClientId: string | undefined,
    eventName: string,
    data?: any
) => {
    if (targetClientId) {
        // Emit to specific client if targetClientId is provided
        if (io.sockets.sockets.get(targetClientId)) {
            console.log(
                `Emitting "${eventName}" to socket ID (${targetClientId}).`
            )
            io.to(targetClientId).emit(eventName, data)
        } else {
            console.warn(
                `Failed to emit "${eventName}" because client (${targetClientId}) was not found.`
            )
        }
    } else {
        // Broadcast to all clients if no targetClientId is provided
        console.log(`Broadcasting "${eventName}" to all clients.`)
        io.to(Rooms.Clients).emit(eventName, data)
    }
}
