import { Socket } from 'socket.io'
import { Server as SocketIOServer } from 'socket.io'
import clientStore from '../../services/clientStore'
import { Rooms, DeviceType } from '../../constants'

export function handleConnection(io: SocketIOServer, socket: Socket): void {
    const { clientID, hostname, type } = socket.handshake.query

    // Ensure all required query parameters are present
    if (!type) {
        console.error('Missing required connection parameters:', { type })
        socket.disconnect() // Disconnect if type is missing
        return
    }

    if (socket.recovered) {
        console.log('Client recovered: ', socket.id)
        clientStore.updateClientBySocketID(socket.id, {
            connectionStatus: 'connected',
        })
        clientStore.broadcastClientsToAdmins(io)
        return // Exit early
    }

    switch (type) {
        case DeviceType.Client:
            console.log('Client connected:', socket.id)
            connectClient(io, socket, clientID as string, hostname as string)
            break
        case DeviceType.Admin:
            console.log('Admin connected:', socket.id)
            connectAdmin(socket)
            break
        default:
            console.error('Unknown device type connecting:', type)
            break
    }
}

// MARK: Connect Client
function connectClient(
    io: SocketIOServer,
    socket: Socket,
    clientID: string,
    hostname: string
) {
    // Ensure all required query parameters are present
    if (!hostname || !clientID) {
        console.error('Missing required connection parameters:', {
            clientID,
            hostname,
        })
        socket.disconnect() // Disconnect if parameters are missing
        return
    }

    // Join the clients room
    socket.join(Rooms.Clients)

    // Clean up the hostname if needed
    const cleanHostname = (hostname as string).replace('.local', '')

    const client = clientStore.findClientByHostname(cleanHostname)

    if (client) {
        const socketInfo = {
            ip: socket.handshake.address,
            connectionStatus: 'connected',
            connectedAt: socket.handshake.time,
            socketID: socket.id,
            clientID: clientID as string,
        }

        // Update client with new information
        clientStore.updateClientByHostname(cleanHostname, socketInfo)

        // Notify admins of updated client list
        clientStore.broadcastClientsToAdmins(io)
    } else {
        console.log('Client not found:', cleanHostname)
    }
}

// MARK: Connect Admin
function connectAdmin(socket: Socket) {
    socket.join(Rooms.Admins)
}
