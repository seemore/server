import * as cp from 'child_process'

// Function to run Ansible command and get hostnames in desired JSON format
export function getAnsibleHosts(): Promise<string[]> {
    return new Promise((resolve, reject) => {
        cp.exec(
            'ansible clients -i inventory.ini --list-hosts',
            (error, stdout, stderr) => {
                if (error) {
                    console.error(`Error executing ansible command: ${error}`)
                    reject({ error: `Execution error: ${error.message}` })
                    return
                }

                if (stderr) {
                    console.error(`Ansible stderr: ${stderr}`)
                }

                // Process the output and extract the hosts, then map to desired JSON format
                const hosts = stdout
                    .split('\n')
                    .map((line) => line.trim())
                    .filter((line) => line && !line.startsWith('hosts'))

                resolve(hosts)
            }
        )
    })
}
