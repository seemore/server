import { Server as SocketIOServer, Socket } from 'socket.io'
import clientStore from '../services/clientStore'
import { handleConnection } from './handlers/handleConnection'
import { handleDisconnection } from './handlers/handleDisconnection'
import { handleClientListRefresh } from './handlers/handleClientListRefresh'
import { emitEventToClients } from './handlers/emitEvent'
import { runAnsiblePlaybook } from './handlers/runAnsiblePlaybook'

export async function setupSocketServer(io: SocketIOServer): Promise<void> {
    io.disconnectSockets() // Disconnect all connected clients

    // Client Connected
    io.on('connection', (socket: Socket) => {
        handleConnection(io, socket)

        // MARK: CLIENT EVENTS
        socket.on('clients:list', (callback) => {
            callback(clientStore.getConnectedClients())
        })

        socket.on('clients:list:refresh', () => {
            handleClientListRefresh(io, socket)
        })

        socket.on('clients:identify', (data) => {
            emitEventToClients(io, data.socketID, 'clients:identify')
        })

        socket.on('clients:reconnect', (data, callback) => {
            runAnsiblePlaybook({
                playbook: 'playbooks/reconnect_client.yml',
                hosts: data.hosts,
                callback,
            })
        })

        socket.on('clients:restart', (data, callback) => {
            runAnsiblePlaybook({
                playbook: 'playbooks/restart_device.yml',
                hosts: data.hosts,
                callback,
            })
        })

        socket.on('clients:pull_code', (data, callback) => {
            runAnsiblePlaybook({
                playbook: 'playbooks/pull_client_repo.yml',
                hosts: data.hosts,
                callback,
            })
        })

        socket.on('clients:install_dependencies', (data, callback) => {
            runAnsiblePlaybook({
                playbook: 'playbooks/install_dependencies.yml',
                hosts: data.hosts,
                callback,
            })
        })

        // MARK: LIGHT EVENTS
        socket.on('light:color', (data) => {
            emitEventToClients(io, data.socketID, 'light:color', data.color)
        })

        socket.on('light:demo:start', (data) => {
            emitEventToClients(io, data?.socketID, 'light:demo:start')
        })

        socket.on('light:demo:stop', (data) => {
            emitEventToClients(io, data?.socketID, 'light:demo:stop')
        })

        // MARK: SERVO EVENTS
        socket.on('servo:position', (data) => {
            emitEventToClients(
                io,
                data?.socketID,
                'servo:position',
                data.position
            )
        })

        // MARK: DIAGNOSTICS EVENTS
        socket.on('diagnostics:heartbeat', (data) => {
            clientStore.updateClientBySocketID(socket.id, data)
        })

        socket.on('diagnostics:comprehensive', (callback) => {
            // TODO: Add more diagnostics data
        })

        socket.on("log", (data) => {
            clientStore.updateClientBySocketID(socket.id, data);
        });

        // Client Disconnected
        socket.on('disconnect', () => {
            handleDisconnection(io, socket)
        })
    })

    setInterval(() => {
        clientStore.broadcastClientsToAdmins(io)
    }, 5000)
}
