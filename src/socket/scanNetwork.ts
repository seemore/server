import { exec } from 'child_process'

// Function to run nmap and return array of IP addresses
const scanNetwork = (): Promise<string[]> => {
    return new Promise((resolve, reject) => {
        // Run the nmap command to scan the network
        exec('sudo nmap -sn 10.53.11.0/24', (error, stdout, stderr) => {
            if (error) {
                reject(`Error executing nmap: ${error.message}`)
                return
            }
            if (stderr) {
                reject(`nmap stderr: ${stderr}`)
                return
            }

            const lines = stdout.split('\n')
            const ips: string[] = []

            for (let i = 0; i < lines.length; i++) {
                const line = lines[i].trim()

                // Look for lines that contain "Nmap scan report for"
                if (line.startsWith('Nmap scan report for')) {
                    const ip = line.match(/(\d{1,3}\.){3}\d{1,3}/) // Extract the IP address
                    if (ip) {
                        // Check if the following lines contain 'raspberrypi' in the hostname
                        if (
                            lines[i + 2] &&
                            lines[i + 2].toLowerCase().includes('raspberrypi')
                        ) {
                            ips.push(ip[0]) // Add IP to the list
                        }
                    }
                }
            }

            resolve(ips)
        })
    })
}

// Call the function and log the IP addresses
scanNetwork()
    .then((ips) => {
        console.log('Raspberry Pi devices found:', ips)
    })
    .catch((error) => {
        console.error('Error:', error)
    })
