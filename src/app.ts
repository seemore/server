import express from 'express'
import { router } from './api'
import cors from 'cors'

const app = express()

app.use(cors({
    origin: "http://192.168.0.162:3000",
    methods: ["GET", "POST"],
    credentials: true
}));

// Set up the REST API for game commands
app.use(express.json())
app.use('/api', router)

export default app
