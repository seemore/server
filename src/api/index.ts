import { Router } from 'express'
import { Server } from 'socket.io'
import clientStore from '../services/clientStore'
import { emitEventToClients } from '../socket/handlers/emitEvent'

export const router = Router()

export function setupApi(io: Server) {
    router.get('/clients/list', (req, res) => {
        res.send(clientStore.getConnectedClients())
    })

    router.get('/clients/hosts', (req, res) => {
        res.send(clientStore.getHostList())
    })

    router.post('/light/demo/start', (req, res) => {
        const { socketID } = req.body
        emitEventToClients(io, socketID, 'light:demo:start')
        res.send('Starting Demo')
    })

    router.post('/light/demo/stop', (req, res) => {
        const { socketID } = req.body
        emitEventToClients(io, socketID, 'light:demo:stop')
        res.send('Stopping Demo')
    })

    router.post('/light/color', (req, res) => {
        const { socketID, color } = req.body
        if (!color) {
            res.status(400).send('Color is required')
            return
        }
        emitEventToClients(io, socketID, 'light:color', color)
        res.send('Color set successfully: ' + req.body.color)
    })

    router.post('/servo/position', (req, res) => {
        const { socketID, position } = req.body
        if (position == null) {
            res.status(400).send('Position is required')
            return
        }
        emitEventToClients(io, socketID, 'servo:position', position)
        res.send(`Moving servos to position ${position}`)
    })

    // Test route for Sweep3d
    router.post('/sweep3d', (req, res) => {
        const { frameNumber, syncTimestamp, frames } = req.body

        console.log(`Received frame ${frameNumber} with syncTimestamp ${syncTimestamp}: ${frames}`)

        res.status(200).send('Frame received')

    })

}
