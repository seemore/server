export type DeviceInfo = {
    hostname: string
    clientID?: string
    socketID?: string
    ip?: string
    connectedAt?: string
    connectionStatus?: string
    userAgent?: string
    gitLastPullTimestamp?: Date
    diagnostics?: {
        cpuLoad?: number // Unit: percentage
        memoryUsage?: {
            used?: number // Unit: MB
            total?: number // Unit: MB
        }
        diskUsage?: {
            used?: number // Unit: MB
            size?: number // Unit: MB
        }
        temp?: number // Unit: Celsius
        uptime?: number // Unit: seconds
    }
    logs? : {
        timestamp: string;
        message: string;
    }[]
}
