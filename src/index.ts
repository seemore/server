import app from './app'
import http from 'http'
import { Server as SocketIOServer } from 'socket.io'
import { setupSocketServer } from './socket'
import { setupApi } from './api'
import clientStore from './services/clientStore'

async function startServer() {
    await clientStore.init()

    const server = http.createServer(app)
    const io = new SocketIOServer(server, {
        cors: {
            origin: "*", // Allow client origin, can be an array for multiple origins
            credentials: true,
        },
        transports: ["websocket", "polling"]
    })

    // Set up the Socket.IO server
    setupSocketServer(io)

    // Set up the API and pass the Socket.IO server instance to it
    setupApi(io)

    const PORT = 4000
    server.listen(PORT, "0.0.0.0", () => {
        console.log(`Server is running on port ${PORT}`)
    })
}

startServer().catch((err) => {
    console.error('Error starting the server:', err)
})
